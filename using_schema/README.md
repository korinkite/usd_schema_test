## How To Build

```bash
export USD_ROOT_PATH=/usr/local/USD
export LIBRARY_PATH=$USD_ROOT_PATH/lib
cmake ..
make
```

Note: Update the `export` commands to point to the correct locations
